<?php

namespace Modules\AdminSetting\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Session;
use App\Utils\Options;
use App\Utils\Permission;
use Input;
use Auth;




class AdminSettingController extends Controller
{
    /**
     * GENERAL SETTINGS
     */
    public function general()
    {
        //if ( !Permission::checkPermission( 'general' ) )
          //  return redirect()->route('access-forbidden');

        $data=array();
        $data['breadcrumbs']='<li><a href="'.route('admin.dashboard').'">Home</a></li><li>General Configurations</li>';
        $data['title']="General Configurations - ".\Options::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'general';
        return view('adminsetting::index',$data);
    }

    public function generalStore(Request $request)
    {
       // if ( !Permission::checkPermission( 'general' ) )
          //  return redirect()->route('access-forbidden');

        $request->validate([
            'system_name'=>'required',
            'system_slogan'=>'required',
            'system_email'=>'required',
            'system_feedback_email'=>'required',
            'system_telephone_no'=>'required',
            'system_address'=>'required',
            'system_mobile' => 'required'
        ]);

        // site config data
        $siteconfig = [
            'system_name' => $request->get('system_name'),
            'system_email' => $request->get('system_email'),
            'system_slogan' => $request->get('system_slogan'),
            'system_address' => $request->get('system_address'),

        ];

        Options::update('siteconfig', $siteconfig);
        Options::update('system_feedback_email', $request->get('system_feedback_email'));
        Options::update('system_telephone_no', $request->get('system_telephone_no'));
        Options::update('system_mobile', $request->get('system_mobile'));

        Options::update('outlets_verification_email1', $request->get('outlets_verification_email1'));
        Options::update('outlets_verification_email2', $request->get('outlets_verification_email2'));
        Options::update('system_address', $request->get('system_address'));

        if($request->hasFile('logo'))
        {
            $image = $request->file('logo');
            $brand_image  = time() . '-' .rand(111111,999999).'.'.$image->getClientOriginalExtension();

            $path = public_path()."/uploads/config/";

            $image->move($path,$brand_image);
            Options::update('brand_image', $brand_image);
        }

        if($request->hasFile('brand_image_footer'))
        {
            $image = $request->file('brand_image_footer');
            $brand_image_footer  = time() . '-' .rand(111111,999999).'.'.$image->getClientOriginalExtension();

            $path = public_path()."/uploads/config/";

            $image->move($path,$brand_image_footer);
            Options::update('brand_image_footer', $brand_image_footer);
        }

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('admin.setting.general');
    }

    /**
     * SOCIAL SETTINS
     */
    public function social()
    {
        //if ( !Permission::checkPermission( 'social-media' ) )
           // return redirect()->route('access-forbidden');

        $data=array();
        $data['breadcrumbs']='<li><a href="'.route('admin.dashboard').'">Home</a></li><li>Social Media Configurations</li>';
        $data['title']="Social Media Configurations - ".\Options::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'social';
        return view('adminsetting::social',$data);
    }

    public function socialStore(Request $request)
    {
        //if ( !Permission::checkPermission( 'social-media' ) )
           // return redirect()->route('access-forbidden');

       $request->get('facebook_url') ? Options::update('facebook_url', $request->get('facebook_url')) : '';
       $request->get('twitter_url') ? Options::update('twitter_url', $request->get('twitter_url')) : '';
       $request->get('google_plus_url') ? Options::update('google_plus_url', $request->get('google_plus_url')) : '';
       $request->get('instagram_url') ? Options::update('instagram_url', $request->get('instagram_url')) : '';
       $request->get('youtube_url') ? Options::update('youtube_url', $request->get('youtube_url')) : '';
       $request->get('viber') ? Options::update('viber', $request->get('viber')) : '';
       $request->get('whatsapp') ? Options::update('whatsapp', $request->get('whatsapp')) : '';

       Session::flash('success_message', 'Records updated successfully.');
       return redirect()->route('admin.setting.social');
    }

    /**
     * META SETTINS
     */
    public function meta()
    {
        //if ( !Permission::checkPermission( 'meta-settings' ) )
           // return redirect()->route('access-forbidden');

        $data=array();
        $data['breadcrumbs']='<li><a href="'.route('admin.dashboard').'">Home</a></li><li>Meta Settings</li>';
        $data['title']="Meta Settings - ".\Options::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'meta';
        return view('adminsetting::meta',$data);
    }

    public function metaStore(Request $request)
    {
        //if ( !Permission::checkPermission( 'meta-settings' ) )
            //return redirect()->route('access-forbidden');

        $request->get('meta_title') ? Options::update('meta_title', $request->get('meta_title')) : '';
        $request->get('meta_description') ? Options::update('meta_description', $request->get('meta_description')) : '';
        $request->get('meta_keyword') ? Options::update('meta_keyword', $request->get('meta_keyword')) : '';

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('admin.setting.meta');
    }

    /**
     * IMAGE DIMENSION
     */
    public function imgDimension()
    {
        //if ( !Permission::checkPermission( 'image-dimensions' ) )
           // return redirect()->route('access-forbidden');

        $data=array();
        $data['breadcrumbs']='<li><a href="'.route('admin.dashboard').'">Home</a></li><li>Image Dimension Settings</li>';
        $data['title']="Image Dimension Settings - ".\Options::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'image';
        return view('adminsetting::image',$data);
    }

    public function imgDimensionStore(Request $request)
    {
        //if ( !Permission::checkPermission( 'image-dimensions' ) )
           // return redirect()->route('access-forbidden');

        $request->get('small_banner_width') ? Options::update('small_banner_width', $request->get('small_banner_width')) : '';
        $request->get('small_banner_height') ? Options::update('small_banner_height', $request->get('small_banner_height')) : '';
        $request->get('bottom_offer_width') ? Options::update('bottom_offer_width', $request->get('bottom_offer_width')) : '';
        $request->get('bottom_offer_height') ? Options::update('bottom_offer_height', $request->get('bottom_offer_height')) : '';

        $request->get('top_ads_width') ? Options::update('top_ads_width', $request->get('top_ads_width')) : '';
        $request->get('top_ads_height') ? Options::update('top_ads_height', $request->get('top_ads_height')) : '';
        $request->get('bottom_ads_width') ? Options::update('bottom_ads_width', $request->get('bottom_ads_width')) : '';
        $request->get('bottom_ads_height') ? Options::update('bottom_ads_height', $request->get('bottom_ads_height')) : '';
        $request->get('side_ads_width') ? Options::update('side_ads_width', $request->get('side_ads_width')) : '';
        $request->get('side_ads_height') ? Options::update('side_ads_height', $request->get('side_ads_height')) : '';

        Session::flash('success_message', 'Records updated successfully.');
        return redirect()->route('admin.setting.img-dimension');
    }

    
}
