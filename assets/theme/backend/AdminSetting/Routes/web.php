<?php

Route::group(['middleware' => ['web', 'auth-admin'], 'prefix' => 'admin/setting', 'namespace' => 'Modules\AdminSetting\Http\Controllers'], function()
{

    // GENERAL SETTINGS
    Route::get('general',array(
        'as'=>'admin.setting.general',
        'uses'=>'AdminSettingController@general'
    ));

    Route::post('general/store',array(
        'as'=>'admin.setting.general.store',
        'uses'=>'AdminSettingController@generalStore'
    ));

    // SOCIAL MEDIA SETTINGS
    Route::get('social',array(
        'as'=>'admin.setting.social',
        'uses'=>'AdminSettingController@social'
    ));

    Route::post('social/store',array(
        'as'=>'admin.setting.social.store',
        'uses'=>'AdminSettingController@socialStore'
    ));

    // META SETTINGS
    Route::get('meta',array(
        'as'=>'admin.setting.meta',
        'uses'=>'AdminSettingController@meta'
    ));

    Route::post('meta/store',array(
        'as'=>'admin.setting.meta.store',
        'uses'=>'AdminSettingController@metaStore'
    ));

    // IMAGE DIMENSION SETTINGS
    Route::get('img-dimension',array(
        'as'=>'admin.setting.img-dimension',
        'uses'=>'AdminSettingController@imgDimension'
    ));

    Route::post('img-dimension/store',array(
        'as'=>'admin.setting.img-dimension.store',
        'uses'=>'AdminSettingController@imgDimensionStore'
    ));

   

});
