@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <!--Panel heading-->
                @include('backend.common.panel-heading-settings')

                <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                         <form action="{{ route('admin.setting.meta.store') }}" method="POST" class="panel-body form-horizontal form-padding">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="admin-first-name">Meta Title</label>
                            <div class="col-md-9">
                                <textarea name="meta_title" class="form-control" placeholder="Meta Title" >{{ old('meta_title') ? old('meta_title') : Options::get('meta_title') }}</textarea>
                                <small class="help-block text-danger">{{$errors->first('meta_title')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="admin-first-name">Meta Description</label>
                            <div class="col-md-9">
                                <textarea name="meta_description" class="form-control" placeholder="Meta Descriptions" rows="8" >{{ old('meta_description') ? old('meta_description') : Options::get('meta_description') }}</textarea>
                                <small class="help-block text-danger">{{$errors->first('meta_description')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="admin-first-name">Meta Keywords</label>
                            <div class="col-md-9">
                                <textarea name="meta_keyword" class="form-control" placeholder="Meta Keyword" rows="8">{{ old('meta_keyword') ? old('meta_keyword') : Options::get('meta_keyword') }}</textarea>
                                <small class="help-block text-danger">{{$errors->first('meta_keyword')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                            </div>
                        </div>
                    </div>

                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop