@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <!--Panel heading-->
            @include('backend.common.panel-heading-settings')

            <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                            <form action="{{ route('admin.setting.riya.store') }}" method="POST" class="panel-body form-horizontal form-padding">
                                {{ csrf_field() }}

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="form-group" style="margin-left: 35px;text-transform: uppercase;">
                                        <label class="col-md-6 control-label" for="admin-first-name">Test Parameters</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test WebTerminal</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_test_webterminal" class="form-control" value="{{ old('riya_test_webterminal') ? old('riya_test_webterminal') : Options::get('riya_test_webterminal') }}" placeholder="Test WebTerminal">
                                            <small class="help-block text-danger">{{$errors->first('riya_test_webterminal')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test WebUserName</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_test_webusername" class="form-control" value="{{ old('riya_test_webusername') ? old('riya_test_webusername') : Options::get('riya_test_webusername') }}" placeholder="Test WebUsername" >
                                            <small class="help-block text-danger">{{$errors->first('riya_test_webusername')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test WebPassword</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_test_webpassword" class="form-control" value="{{ old('riya_test_webpassword') ? old('riya_test_webpassword') : Options::get('riya_test_webpassword') }}" placeholder="Test WebPassword" >
                                            <small class="help-block text-danger">{{$errors->first('riya_test_webpassword')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test WebIp 1</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_test_webip1" class="form-control" value="{{ old('riya_test_webip1') ? old('riya_test_webip1') : Options::get('riya_test_webip1') }}" placeholder="Test WebIp 1" >
                                            <small class="help-block text-danger">{{$errors->first('riya_test_webip1')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test WebIp 2</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_test_webip2" class="form-control" value="{{ old('riya_test_webip2') ? old('riya_test_webip2') : Options::get('riya_test_webip2') }}" placeholder="Test WebIp 2" >
                                            <small class="help-block text-danger">{{$errors->first('riya_test_webip2')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="demo-contact-input">Current Mode</label>
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input id="status" class="magic-radio" type="radio" name="riya_mode" value="test" {{ Options::get('riya_mode') == 'test' ? "checked" : "" }}>
                                                <label for="status" style="color:#000000;">TEST</label>
                                                <input id="status-2" class="magic-radio" type="radio" name="riya_mode" value="live" {{ Options::get('riya_mode') == 'live' ? "checked" : "" }}>
                                                <label for="status-2" style="color:#000000;">LIVE</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="form-group" style="margin-left: 115px;text-transform: uppercase;">
                                        <label class="col-md-5 control-label" for="admin-first-name">Live Parameters</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Live WebTerminal</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_live_webterminal" class="form-control" value="{{ old('riya_live_webterminal') ? old('riya_live_webterminal') : Options::get('riya_live_webterminal') }}" placeholder="Live WebTerminal">
                                            <small class="help-block text-danger">{{$errors->first('riya_live_webterminal')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Live WebUserName</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_live_webusername" class="form-control" value="{{ old('riya_live_webusername') ? old('riya_live_webusername') : Options::get('riya_live_webusername') }}" placeholder="Live WebUsername" >
                                            <small class="help-block text-danger">{{$errors->first('riya_live_webusername')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Live WebPassword</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_live_webpassword" class="form-control" value="{{ old('riya_live_webpassword') ? old('riya_live_webpassword') : Options::get('riya_live_webpassword') }}" placeholder="Live WebPassword" >
                                            <small class="help-block text-danger">{{$errors->first('riya_live_webpassword')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Live WebIp 1</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_live_webip1" class="form-control" value="{{ old('riya_live_webip1') ? old('riya_live_webip1') : Options::get('riya_live_webip1') }}" placeholder="Live WebIp 1" >
                                            <small class="help-block text-danger">{{$errors->first('riya_live_webip1')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Live WebIp 2</label>
                                        <div class="col-md-6">
                                            <input type="text" name="riya_live_webip2" class="form-control" value="{{ old('riya_live_webip2') ? old('riya_live_webip2') : Options::get('riya_live_webip2') }}" placeholder="Live WebIp 2" >
                                            <small class="help-block text-danger">{{$errors->first('riya_live_webip2')}}</small>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix"></div>
                                <hr>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop