@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif


            <div class="panel">

                <!--Panel heading-->
            @include('backend.common.panel-heading-settings')

            <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                            <form action="{{ route('admin.setting.commission.store') }}" method="POST" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">SELECT USER TYPE</label>
                                        <div class="col-md-2">
                                            <select class="form-control" name="user_type" id="user_type">
                                                <option value="individual" {{ $user_type == 'individual' ? "selected='selected'" : "" }}>INDIVIDUAL USER</option>
                                                <option value="outlets" {{ $user_type == 'outlets' ? "selected='selected'" : "" }}>OUTLETS</option>
                                                <option value="agents" {{ $user_type == 'agents' ? "selected='selected'" : "" }}>TRAVEL AGENTS</option>
                                                <option value="corporate" {{ $user_type == 'corporate' ? "selected='selected'" : "" }}>CORPORATES</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="demo-contact-input">Commission Type</label>
                                        <div class="col-md-8">
                                            <div class="radio">
                                                <input id="status-2" class="magic-radio commission_type" type="radio" name="commission_type" value="flat"
                                                        {{ isset( $general_comm->commission_type ) && $general_comm->commission_type == 'flat' ? 'checked' : "checked" }}
                                                >
                                                <label for="status-2" style="color:#000000;">Flat</label>
                                                <input id="status" class="magic-radio commission_type" type="radio" name="commission_type" value="percent"
                                                        {{ isset( $general_comm->commission_type ) && $general_comm->commission_type == 'percent' ? 'checked' : "" }}
                                                >
                                                <label for="status" style="color:#000000;">Percentage( % )</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Nepalese Currency (NPR):</label>
                                        <div class="col-md-3">
                                            <input type="text" name="commission_npr" class="form-control" value="{{ isset( $general_comm->commission_npr ) ? $general_comm->commission_npr : "" }}">
                                            <small class="help-block text-danger">{{$errors->first('commission_npr')}}</small>
                                        </div>
                                        <div class="col-md-4 text-bold">
                                            <small class="help-block text-danger info_container_npr">
                                                @if( isset( $general_comm->commission_type ) && $general_comm->commission_type == 'percent' )
                                                    ( Commission % for NPR )
                                                @else
                                                    ( Flat amount for NPR )
                                                @endif
                                            </small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">US Dollar (USD):</label>
                                        <div class="col-md-3">
                                            <input type="text" name="commission_usd" class="form-control" value="{{ isset( $general_comm->commission_usd ) ? $general_comm->commission_usd : "" }}">
                                            <small class="help-block text-danger">{{$errors->first('commission_usd')}}</small>
                                        </div>
                                        <div class="col-md-4 text-bold">
                                            <small class="help-block text-danger info_container_usd">
                                                @if( isset( $general_comm->commission_type ) && $general_comm->commission_type == 'percent' )
                                                    ( Commission % for USD )
                                                @else
                                                    ( Flat amount for USD )
                                                @endif
                                            </small>
                                        </div>
                                    </div>

                                    {{--<div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Discount:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="discount" class="form-control" value="{{ isset( $general_comm->discount ) ? $general_comm->discount : "" }}">
                                            <small class="help-block text-danger">{{$errors->first('discount')}}</small>
                                        </div>
                                        <div class="col-md-4 text-bold">
                                            <small class="help-block text-danger">( Discount in % )</small>
                                        </div>
                                    </div>--}}

                                    @if( $user_type == 'outlets' )
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Outlets Max. Commission:</label>
                                        <div class="col-md-3">
                                            <input type="text" name="outlets_max_commission" class="form-control" value="{{ Options::get('outlets_max_commission') }}">
                                            <small class="help-block text-danger">{{$errors->first('outlets_max_commission')}}</small>
                                        </div>
                                        <div class="col-md-4 text-bold">
                                            <small class="help-block text-danger">( Max. Commission Outlets can set from their profile )</small>
                                        </div>
                                    </div>
                                    @elseif( $user_type == 'agents' )
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="admin-first-name">Agents Max. Commission:</label>
                                                <div class="col-md-3">
                                                    <input type="text" name="agents_max_commission" class="form-control" value="{{ Options::get('agents_max_commission') }}">
                                                    <small class="help-block text-danger">{{$errors->first('agents_max_commission')}}</small>
                                                </div>
                                                <div class="col-md-4 text-bold">
                                                    <small class="help-block text-danger">( Max. Commission Agents can set from their profile )</small>
                                                </div>
                                            </div>
                                    @elseif( $user_type == 'corporate' )
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="admin-first-name">Corporates Max. Commission:</label>
                                                <div class="col-md-3">
                                                    <input type="text" name="corporate_max_commission" class="form-control" value="{{ Options::get('corporate_max_commission') }}">
                                                    <small class="help-block text-danger">{{$errors->first('corporate_max_commission')}}</small>
                                                </div>
                                                <div class="col-md-4 text-bold">
                                                    <small class="help-block text-danger">( Max. Commission Corporates can set from their profile )</small>
                                                </div>
                                            </div>
                                    @endif

                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-3">
                                            <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">


                                    {{--<div style="color: #2b425b !important;font-weight: 700 !important;">
                                        <span>
                                            Extra Discounts
                                        </span>

                                        <a href="javascript:void(0);" id="btn-discount-add" class="btn btn-info pull-right" style="margin-bottom: 5px;">
                                            <i class="fa fa-plus"></i> Add New
                                        </a>

                                    </div>
                                    <div class="clearfix"></div>--}}

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Default Baggage(kgs)</label>
                                        <div class="col-md-6">
                                            <input type="number" name="default_baggage" class="form-control" value="{{ old('default_baggage') ? old('default_baggage') : Options::get('default_baggage') }}" placeholder="Default Baggage" >
                                            {{--<small class="help-block text-danger">{{$errors->first('default_baggage')}}</small>--}}
                                            <small class="help-block text-danger">( Used when GDS doesnt return baggage details )</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Default Handcarry(kgs)</label>
                                        <div class="col-md-6">
                                            <input type="number" name="default_handcarry" class="form-control" value="{{ old('default_handcarry') ? old('default_handcarry') : Options::get('default_handcarry') }}" placeholder="Default Handcarry" >
                                            <small class="help-block text-danger">( Used when GDS doesnt return handcarry details )</small>
                                        </div>
                                    </div>


                                    {{--<table class="table  table-bordered adminMgmtTable">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Type</th>
                                            <th class="text-center">Carriers</th>
                                            <th class="text-center">NPR</th>
                                            <th class="text-center">USD</th>
                                            <th class="text-center">Status</th>
                                            <th style="width: 120px;">Operation</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if( count($discounts) > 0 )
                                            @foreach( $discounts as $discount )
                                                <tr>
                                                    <td class="text-center">{{ ucfirst($discount->discount_type) }}</td>
                                                    <td class="text-center">{{ strtoupper($discount->carrier) }}</td>
                                                    <td class="text-center">{{ $discount->discount_npr }}</td>
                                                    <td class="text-center">{{ $discount->discount_usd }}</td>
                                                    <td class="text-center">
                                                        {!!  $discount->status == '1' ? '<strong style="color:green;">Active</strong>' : '<strong style="color:#bf302f;">Inactive</strong>' !!}
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="javascript:void(0);" class="btn btn-info btn-update-discount" title="Edit Discount"
                                                           data-discount-id="{{ $discount->id }}"
                                                           data-discount-carrier="{{ $discount->carrier }}"
                                                           data-discount-type="{{ $discount->discount_type }}"
                                                           data-discount-npr="{{ $discount->discount_npr }}"
                                                           data-discount-usd="{{ $discount->discount_usd }}"
                                                           data-discount-status="{{ $discount->status }}"
                                                        >
                                                            <i class="fa fa-edit"></i>
                                                        </a>

                                                        <a href="{{ route('admin.setting.discount.delete',[$discount->id]) }}" class="btn btn-danger" data-toggle="confirmation">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>--}}


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Discount -->
    <div id="discount-add-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title" id="myLargeModalLabel">Add Discount Rule</h4>
                </div>

                <form class="form-horizontal" action="{{ route('admin.setting.discount.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="bootbox-body">
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Carrier</label>
                                        <div class="col-md-6">
                                            <select name="carrier" class="form-control input-md">
                                                <option value="all">ALL CARRIERS</option>
                                                @if( count($carriers)>0 )
                                                    @foreach( $carriers as $car )
                                                        <option value="{{ $car->company }}">{{ $car->company." - ".$car->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Discount Type</label>
                                        <div class="col-md-6">
                                            <div class="radio pull-left">
                                                <input id="discount-type-flat" class="magic-radio" type="radio" name="discount_type" value="flat" checked>
                                                <label for="discount-type-flat" style="color:#000000;">Flat</label>
                                                <input id="discount-type-percent" class="magic-radio" type="radio" name="discount_type" value="percent">
                                                <label for="discount-type-percent" style="color:#000000;">Percent</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Nepalese Currency (NPR)</label>
                                        <div class="col-md-6">
                                            <input name="discount_npr" type="text" class="form-control input-md" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">US Dollar (USD)</label>
                                        <div class="col-md-6">
                                            <input name="discount_usd" type="text" class="form-control input-md" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Status</label>
                                        <div class="col-md-6">
                                            <div class="radio pull-left">
                                                <input id="discount-status" class="magic-radio" type="radio" name="status" value="1" checked>
                                                <label for="discount-status" style="color:#000000;">Active</label>
                                                <input id="discount-status-inactive" class="magic-radio" type="radio" name="status" value="0">
                                                <label for="discount-status-inactive" style="color:#000000;">Inactive</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-purple">Create</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div id="discount-update-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title" id="myLargeModalLabel">Update Discount Rule</h4>
                </div>

                <form class="form-horizontal" action="{{ route('admin.setting.discount.update') }}" method="POST">
                    {{ csrf_field() }}

                    <input type="hidden" name="_discount_id" id="_discount_id">
                    <input type="hidden" name="_old_carrier_id" id="_old_carrier_id">

                    <div class="modal-body">
                        <div class="bootbox-body">
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Carrier</label>
                                        <div class="col-md-6">
                                            <select name="carrier" class="form-control input-md" id="_discount_carrier">
                                                <option value="all">ALL CARRIERS</option>
                                                @if( count($carriers)>0 )
                                                    @foreach( $carriers as $car )
                                                        <option value="{{ $car->company }}">{{ $car->company." - ".$car->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Discount Type</label>
                                        <div class="col-md-6">
                                            <div class="radio pull-left">
                                                <input id="discount-type-flat-update" class="magic-radio" type="radio" name="discount_type_update" value="flat" checked>
                                                <label for="discount-type-flat-update" style="color:#000000;">Flat</label>
                                                <input id="discount-type-percent-update" class="magic-radio" type="radio" name="discount_type_update" value="percent">
                                                <label for="discount-type-percent-update" style="color:#000000;">Percent</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Nepalese Currency (NPR)</label>
                                        <div class="col-md-6">
                                            <input name="discount_npr" type="text" class="form-control input-md" id="_discount_npr">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">US Dollar (USD)</label>
                                        <div class="col-md-6">
                                            <input name="discount_usd" type="text" class="form-control input-md" id="_discount_usd">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Status</label>
                                        <div class="col-md-6">
                                            <div class="radio pull-left">
                                                <input id="discount-status-update" class="magic-radio" type="radio" name="status_update" value="1" checked>
                                                <label for="discount-status-update" style="color:#000000;">Active</label>
                                                <input id="discount-status-inactive-update" class="magic-radio" type="radio" name="status_update" value="0">
                                                <label for="discount-status-inactive-update" style="color:#000000;">Inactive</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-purple">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script>
      $(document).ready(function(){

          $('#user_type').change(function(){

              var user_type = $(this).val();
              var redirect_url = '{{ route('admin.setting.commission') }}';

              if( user_type == "" )
                  return false;

              if( user_type == 'individual' ) {
                  window.location.href = redirect_url;
              } else if ( user_type == 'outlets' ) {
                  redirect_url = redirect_url + "?u_type=outlets";
                  window.location.href = redirect_url;
              } else if ( user_type == 'agents' ) {
              redirect_url = redirect_url + "?u_type=agents";
              window.location.href = redirect_url;
              } else if ( user_type == 'corporate' ) {
                  redirect_url = redirect_url + "?u_type=corporate";
                  window.location.href = redirect_url;
              }

          });

          $('.commission_type').change(function(){

              var commission_type = $(this).val();

              if( commission_type == '' )
                  return false;

              if( commission_type == 'flat' ) {

                  $('.info_container_npr').html('( Flat amount for NPR )');
                  $('.info_container_usd').html('( Flat amount for USD )');

              } else if( commission_type == 'percent' ) {

                  $('.info_container_npr').html('( Commission % for NPR )');
                  $('.info_container_usd').html('( Commission % for USD )');

              }

          });

          $('#btn-discount-add').click(function () {
              $('#discount-add-modal').modal({show: true});
          });

          $('.btn-update-discount').click(function () {
              var discount_id = $(this).attr('data-discount-id');
              if (discount_id == '' || discount_id == null) {
                return false;
              }

              var discount_carrier = $(this).attr('data-discount-carrier');
              var discount_type = $(this).attr('data-discount-type');
              var discount_npr = $(this).attr('data-discount-npr');
              var discount_usd = $(this).attr('data-discount-usd');
              var discount_status = $(this).attr('data-discount-status');

              $('#_discount_id').val(discount_id);
              $('#_discount_carrier').val(discount_carrier);
              $('#_old_carrier_id').val(discount_carrier);

              if ( discount_type === 'flat' ) {
                  $("input[name='discount_type_update'][value='flat']").prop('checked', true);
              } else if( discount_type === 'percent' ) {
                  $("input[name='discount_type_update'][value='percent']").prop('checked', true);
              }

              $('#_discount_npr').val(discount_npr);
              $('#_discount_usd').val(discount_usd);


              if ( discount_status == '1' ) {
                  $("input[name='status_update'][value='1']").prop('checked', true);
              } else if( discount_status == '0' ) {
                  $("input[name='status_update'][value='0']").prop('checked', true);
              }

              $('#discount-update-modal').modal({show: true});
          });



      });
    </script>
@stop