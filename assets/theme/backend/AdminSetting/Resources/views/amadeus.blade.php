@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <!--Panel heading-->
                @include('backend.common.panel-heading-settings')

                <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                            <form action="{{ route('admin.setting.amadeus.store') }}" method="POST" class="panel-body form-horizontal form-padding">
                                {{ csrf_field() }}

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="form-group" style="margin-left: 35px;text-transform: uppercase;">
                                        <label class="col-md-6 control-label" for="admin-first-name">Test Parameters</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test Office ID</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_test_office_id" class="form-control" value="{{ old('amadeus_test_office_id') ? old('amadeus_test_office_id') : Options::get('amadeus_test_office_id') }}" placeholder="Test Office ID" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_test_office_id')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test User ID</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_test_user_id" class="form-control" value="{{ old('amadeus_test_user_id') ? old('amadeus_test_user_id') : Options::get('amadeus_test_user_id') }}" placeholder="Test User ID" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_test_user_id')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Test Password</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_test_password" class="form-control" value="{{ old('amadeus_test_password') ? old('amadeus_test_password') : Options::get('amadeus_test_password') }}" placeholder="Test Password" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_test_password')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="demo-contact-input">Current Mode</label>
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input id="status" class="magic-radio" type="radio" name="amadeus_mode" value="test" {{ Options::get('amadeus_mode') == 'test' ? "checked" : "" }}>
                                                <label for="status" style="color:#000000;">TEST</label>
                                                <input id="status-2" class="magic-radio" type="radio" name="amadeus_mode" value="live" {{ Options::get('amadeus_mode') == 'live' ? "checked" : "" }}>
                                                <label for="status-2" style="color:#000000;">LIVE</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                    <div class="form-group" style="margin-left: 35px;text-transform: uppercase;">
                                        <label class="col-md-5 control-label" for="admin-first-name">Live Parameters</label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="admin-first-name">Live Office ID</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_live_office_id" class="form-control" value="{{ old('amadeus_live_office_id') ? old('amadeus_live_office_id') : Options::get('amadeus_live_office_id') }}" placeholder="Live Office ID" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_live_office_id')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="admin-first-name">Live User ID</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_live_user_id" class="form-control" value="{{ old('amadeus_live_user_id') ? old('amadeus_live_user_id') : Options::get('amadeus_live_user_id') }}" placeholder="Live User ID" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_test_user_id')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="admin-first-name">Test Password</label>
                                        <div class="col-md-6">
                                            <input type="text" name="amadeus_live_password" class="form-control" value="{{ old('amadeus_live_password') ? old('amadeus_live_password') : Options::get('amadeus_live_password') }}" placeholder="Live Password" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_live_password')}}</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">No. Of Recommendations</label>
                                        <div class="col-md-3">
                                            <input type="text" name="amadeus_no_recommendations" class="form-control" value="{{ old('amadeus_no_recommendations') ? old('amadeus_no_recommendations') : Options::get('amadeus_no_recommendations') }}" placeholder="eg. 10,15,20" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_no_recommendations')}}</small>
                                        </div>
                                        <div class="col-md-7 text-bold">
                                            <small class="help-block text-danger info_container_npr">( No. of flight results to display after flight search )</small>
                                        </div>
                                    </div>

                                    {{--<div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">Exchange Charge</label>
                                        <div class="col-md-3">
                                            <input type="text" name="amadeus_exchange_charge" class="form-control" value="{{ old('amadeus_exchange_charge') ? old('amadeus_exchange_charge') : Options::get('amadeus_exchange_charge') }}" placeholder="Exchange Charge" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_exchange_charge')}}</small>
                                        </div>
                                        <div class="col-md-7 text-bold">
                                            <small class="help-block text-danger info_container_npr">( Used in eTicket Charges section )</small>
                                        </div>
                                    </div>--}}

                                    {{--<div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">Refund Charge</label>
                                        <div class="col-md-3">
                                            <input type="text" name="amadeus_refund_charge" class="form-control" value="{{ old('amadeus_refund_charge') ? old('amadeus_refund_charge') : Options::get('amadeus_refund_charge') }}" placeholder="Refund Charge" >
                                            <small class="help-block text-danger">{{$errors->first('amadeus_refund_charge')}}</small>
                                        </div>
                                        <div class="col-md-7 text-bold">
                                            <small class="help-block text-danger info_container_npr">( Used in eTicket Charges section )</small>
                                        </div>
                                    </div>--}}

                                    {{--<div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">Default Baggage(kgs)</label>
                                        <div class="col-md-3">
                                            <input type="number" name="default_baggage" class="form-control" value="{{ old('default_baggage') ? old('default_baggage') : Options::get('default_baggage') }}" placeholder="Default Baggage" >
                                            <small class="help-block text-danger">{{$errors->first('default_baggage')}}</small>
                                        </div>
                                        <div class="col-md-7 text-bold">
                                            <small class="help-block text-danger info_container_npr">( Used when GDS doesnt return baggage details )</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="admin-first-name">Default Handcarry(kgs)</label>
                                        <div class="col-md-3">
                                            <input type="number" name="default_handcarry" class="form-control" value="{{ old('default_handcarry') ? old('default_handcarry') : Options::get('default_handcarry') }}" placeholder="Default Handcarry" >
                                            <small class="help-block text-danger">{{$errors->first('default_handcarry')}}</small>
                                        </div>
                                        <div class="col-md-7 text-bold">
                                            <small class="help-block text-danger info_container_npr">( Used when GDS doesnt return handcarry details )</small>
                                        </div>
                                    </div>--}}

                                    {{--
                                                                        <div class="clearfix"></div>
                                                                        <hr>

                                                                        <div class="form-group" style="text-transform: uppercase;">
                                                                            <label class="col-md-2 control-label"></label>
                                                                            <label class="col-md-9 control-label text-left">SPECIAL FLIGHT CASES</label>
                                                                        </div>

                                                                        <div class="form-group" style="margin-top: -10px;">
                                                                            <label class="col-md-2 control-label" for="admin-first-name"></label>
                                                                            <div class="col-md-9">
                                                                                <div class="checkbox">
                                                                                    <input id="CA-FZ" class="magic-checkbox" type="checkbox" name="display_fz_fourdigit" value="yes" {{ Options::get('display_fz_fourdigit') && Options::get('display_fz_fourdigit') == 'yes' ? 'checked' : ""}}>
                                                                                    <label for="CA-FZ" style="color: #000023;text-transform: uppercase;font-weight: 600;">Display 4 Digit Flights of Fly Dubai (FZ)</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>--}}

                                    <div class="clearfix"></div>
                                    <hr>

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop