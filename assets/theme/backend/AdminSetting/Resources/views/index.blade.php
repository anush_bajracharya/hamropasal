@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span
                                class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span
                                class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif


            <div class="panel">

                <!--Panel heading-->
            @include('backend.common.panel-heading-settings')

            <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                            <form action="{{ route('admin.setting.general.store') }}" method="POST"
                                  class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">System Name <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_name" class="form-control"
                                                   value="{{ old('system_name') ? old('system_name') : Options::get('siteconfig')['system_name'] }}"
                                                   placeholder="System Name">
                                            <small class="help-block text-danger">{{$errors->first('system_name')}}</small>
                                        </div>{{--$_CONFIG['admin_name'];--}}
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Slogan <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_slogan" class="form-control"
                                                   value="{{ old('system_slogan') ? old('system_slogan') : Options::get('siteconfig')['system_slogan'] }}"
                                                   placeholder="System Slogan">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('system_slogan')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Address <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_address" class="form-control"
                                                   value="{{ old('system_address') ? old('system_address') : Options::get('siteconfig')['system_address'] }}"
                                                   placeholder="System Address">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('system_address')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Admin Email <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_email" class="form-control"
                                                   value="{{ old('system_email') ? old('system_email') : Options::get('siteconfig')['system_email'] }}"
                                                   placeholder="System Email">
                                            <small class="help-block text-danger">{{$errors->first('system_email')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Feedback/Support Email
                                            <span class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_feedback_email" class="form-control"
                                                   value="{{ old('system_feedback_email') ? old('system_feedback_email') : Options::get('system_feedback_email') }}"
                                                   placeholder="Feedback Email">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('system_feedback_email')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Telephone No. <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_telephone_no" class="form-control"
                                                   value="{{ old('system_telephone_no') ? old('system_telephone_no') : Options::get('system_telephone_no') }}"
                                                   placeholder="Telephone Number">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('system_telephone_no')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Mobile No. <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" name="system_mobile" class="form-control"
                                                   value="{{ old('system_mobile') ? old('system_mobile') : Options::get('system_mobile') }}"
                                                   placeholder="Mobile Number">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('system_mobile')}}</small>
                                        </div>
                                    </div>

                                    @if( Options::get('brand_image') && Options::get('brand_image') != "" )
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <img src="{{ asset('uploads/config/'.Options::get('brand_image')) }}">
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Brand Logo <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="file" name="logo" class="form-control">
                                            <small class="help-block text-danger">{{$errors->first('logo')}}</small>
                                        </div>
                                    </div>

                                    @if( Options::get('brand_image_footer') && Options::get('brand_image_footer') != "" )
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <img src="{{ asset('uploads/config/'.Options::get('brand_image_footer')) }}">
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Brand Logo Footer <span
                                                    class="required_color">*</span></label>
                                        <div class="col-md-8">
                                            <input type="file" name="brand_image_footer" class="form-control">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('brand_image_footer')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Outlets Verification
                                            email 1</label>
                                        <div class="col-md-8">
                                            <input type="text" name="outlets_verification_email1" class="form-control"
                                                   value="{{ old('outlets_verification_email1') ? old('outlets_verification_email1') : Options::get('outlets_verification_email1') }}"
                                                   placeholder="Verification email">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('outlets_verification_email1')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="admin-first-name">Outlets Verification
                                            email 2</label>
                                        <div class="col-md-8">
                                            <input type="text" name="outlets_verification_email2" class="form-control"
                                                   value="{{ old('outlets_verification_email2') ? old('outlets_verification_email2') : Options::get('outlets_verification_email2') }}"
                                                   placeholder="Verification email">
                                            <small
                                                    class="help-block text-danger">{{$errors->first('outlets_verification_email2')}}</small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4">
                                            <input type="submit" class="btn btn-block btn-primary" name="submit"
                                                   value="UPDATE">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop