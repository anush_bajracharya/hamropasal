@extends('backend.layouts.main')
@section('content')

    <!--Select2 [ OPTIONAL ]-->
    <link href="{{ asset('backend/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <script src="{{ asset('backend/plugins/select2/select2.min.js') }}"></script>


    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
            <a href="{{ route('admin.user.list') }}" class="btn btn-primary breadCrumbRightBackBtn"> <i class="fa fa-chevron-left" aria-hidden="true" style="font-size: 11px;"></i> Back</a>
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Switch Configurations
                        <span class="required_fields"><strong class="required_color">*</strong> These fields are required.</span>
                    </h3>
                </div>

                <form action="{{ route('admin.setting.switch.store') }}" method="POST" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">

                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-3 control-label">Force Maintenance?</label>
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <input id="force-1" class="magic-checkbox" type="checkbox" name="force_maintainance_status" value="yes" {{ Options::get('force_maintainance_status') == 'yes' ? "checked" : "" }}>
                                    <label for="force-1" style="color: #000023;">YES</label>
                                </div>
                                <small class="help-block text-danger">{{$errors->first('groups')}}</small>
                            </div>
                            <div class="col-md-6" style="margin-top: 8px;">
                                <span style="font-weight: 600;color: #b10e14;">
                                     <strong>Warning!</strong> If "Force Maintenance?" is enabled, Frontend will be disabled with "Site Under Maintenance". However frontend is still accesible for debuggin by adding "index.php"
                                </span>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-contact-input">Fare Rule:</label>
                            <div class="col-md-9">
                                <div class="radio">
                                    <input id="fare-rule" class="magic-radio" type="radio" name="fare_rule_type" value="gds_only" {{ Options::get('fare_rule_type') == 'gds_only' ? "checked" : "" }}>
                                    <label for="fare-rule" style="color:#000000;">GDS Fare Only</label>
                                    <input id="fare-rule-2" class="magic-radio" type="radio" name="fare_rule_type" value="gds_commission" {{ Options::get('fare_rule_type') == 'gds_commission' ? "checked" : "" }}>
                                    <label for="fare-rule-2" style="color:#000000;">GDS Fare + Commissions</label>
                                </div>
                                <p style="margin-top: 15px;font-weight: 600;color: #b10e14;">
                                    <strong>GDS Fare + Commissions :</strong> Airlines Specific Commission or General Commission rule will be added.
                                </p>
                            </div>
                        </div>

                        {{--<hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-contact-input">Display:</label>
                            <div class="col-md-9">
                                <div class="radio">
                                    <input id="status" class="magic-radio" type="radio" name="is_convergent_npay" value="convergent" {{ Options::get('is_convergent_npay') == 'convergent' ? "checked" : "" }}>
                                    <label for="status" style="color:#000000;">Convergent</label>
                                    <input id="status-2" class="magic-radio" type="radio" name="is_convergent_npay" value="npay" {{ Options::get('is_convergent_npay') == 'npay' ? "checked" : "" }}>
                                    <label for="status-2" style="color:#000000;">nPay</label>
                                </div>
                            </div>
                        </div>--}}

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-contact-input">Enable Amadeus API:</label>
                            <div class="col-md-3">
                                <div class="radio">
                                    <input id="is_active_amadeus" class="magic-radio" type="radio" name="is_active_amadeus" value="yes" {{ Options::get('is_active_amadeus') == 'yes' ? "checked" : "" }}>
                                    <label for="is_active_amadeus" style="color:#000000;">ENABLE</label>
                                    <input id="is_active_amadeus-2" class="magic-radio" type="radio" name="is_active_amadeus" value="no" {{ Options::get('is_active_amadeus') == 'no' ? "checked" : "" }}>
                                    <label for="is_active_amadeus-2" style="color:#000000;">DISABLE</label>
                                </div>
                            </div>
                            <div class="col-md-6" style="margin-top: 8px;">
                                <span style="font-weight: 600;color: #b10e14;">If enabled, flights from Amadues API will be visible in the application.</span>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-contact-input">Enable Riya API:</label>
                            <div class="col-md-3">
                                <div class="radio">
                                    <input id="is_active_riya" class="magic-radio" type="radio" name="is_active_riya" value="yes" {{ Options::get('is_active_riya') == 'yes' ? "checked" : "" }}>
                                    <label for="is_active_riya" style="color:#000000;">ENABLE</label>
                                    <input id="is_active_riya-2" class="magic-radio" type="radio" name="is_active_riya" value="no" {{ Options::get('is_active_riya') == 'no' ? "checked" : "" }}>
                                    <label for="is_active_riya-2" style="color:#000000;">DISABLE</label>
                                </div>
                            </div>
                            <div class="col-md-6" style="margin-top: 8px;">
                                @php
                                    $riya_airlines = Options::get('riya_airlines') ? json_decode(Options::get('riya_airlines'),TRUE) : [];
                                @endphp

                                <div class="form-group">

                                    <div class="col-md-12 text-left">
                                        <select id="multi-airlines-riya" multiple="multiple" class="col-md-12 form-control" name="riya_airlines[]">
                                            @if( $carriers->count() > 0 )
                                                @foreach ( $carriers as $carrier )
                                                    <option value="{{ $carrier->company }}" {{ in_array($carrier->company,$riya_airlines) ? "selected='selected'" : ""  }}>{{ $carrier->company }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <small style="font-weight: 700;font-style: italic;">Supported Airlines : AI, 9W, 6E</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="demo-contact-input">Enable Galileo API:</label>
                            <div class="col-md-3">
                                <div class="radio">
                                    <input id="is_active_galileo" class="magic-radio" type="radio" name="is_active_galileo" value="yes" {{ Options::get('is_active_galileo') == 'yes' ? "checked" : "" }}>
                                    <label for="is_active_galileo" style="color:#000000;">ENABLE</label>
                                    <input id="is_active_galileo-2" class="magic-radio" type="radio" name="is_active_galileo" value="no" {{ Options::get('is_active_galileo') == 'no' ? "checked" : "" }}>
                                    <label for="is_active_galileo-2" style="color:#000000;">DISABLE</label>
                                </div>
                            </div>
                            <div class="col-md-6" style="margin-top: 8px;">
                                @php
                                    $galileo_airlines_airlines = Options::get('galileo_airlines') ? json_decode(Options::get('galileo_airlines'),TRUE) : [];
                                @endphp

                                <div class="form-group">

                                    <div class="col-md-12 text-left">
                                        <select id="multi-airlines-galileo" multiple="multiple" class="col-md-12 form-control" name="galileo_airlines[]">
                                            @if( $carriers->count() > 0 )
                                                @foreach ( $carriers as $carrier )
                                                    <option value="{{ $carrier->company }}" {{ in_array($carrier->company,$galileo_airlines_airlines) ? "selected='selected'" : ""  }}>{{ $carrier->company }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <small style="font-weight: 700;font-style: italic;">Supported Airlines : AI, 9W, 6E</small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-3">
                                <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("#multi-airlines-galileo,#multi-airlines-riya").select2();

        });
    </script>

@stop