@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <!--Panel heading-->
                @include('backend.common.panel-heading-settings')

                <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                         <form action="{{ route('admin.setting.img-dimension.store') }}" method="POST" class="panel-body form-horizontal form-padding">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Small Banner</label>
                            <div class="col-md-3">
                                <input type="text" name="small_banner_width" class="form-control" value="{{ old('small_banner_width') ? old('small_banner_width') : Options::get('small_banner_width') }}" placeholder="Width" >
                                <small class="help-block text-danger">{{$errors->first('small_banner_width')}}</small>
                            </div>

                            <label class="col-md-1 control-label mr20" for="admin-first-name">X</label>

                            <div class="col-md-3">
                                <input type="text" name="small_banner_height" class="form-control" value="{{ old('small_banner_height') ? old('small_banner_height') : Options::get('small_banner_height') }}" placeholder="Height" >
                                <small class="help-block text-danger">{{$errors->first('small_banner_height')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Bottom Offer</label>
                            <div class="col-md-3">
                                <input type="text" name="bottom_offer_width" class="form-control" value="{{ old('bottom_offer_width') ? old('bottom_offer_width') : Options::get('bottom_offer_width') }}" placeholder="Width" >
                                <small class="help-block text-danger">{{$errors->first('bottom_offer_width')}}</small>
                            </div>

                            <label class="col-md-1 control-label mr20" for="admin-first-name">X</label>

                            <div class="col-md-3">
                                <input type="text" name="bottom_offer_height" class="form-control" value="{{ old('bottom_offer_height') ? old('bottom_offer_height') : Options::get('bottom_offer_height') }}" placeholder="Height" >
                                <small class="help-block text-danger">{{$errors->first('bottom_offer_height')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Top Advertisement</label>
                            <div class="col-md-3">
                                <input type="text" name="top_ads_width" class="form-control" value="{{ old('top_ads_width') ? old('top_ads_width') : Options::get('top_ads_width') }}" placeholder="Width" >
                                <small class="help-block text-danger">{{$errors->first('top_ads_width')}}</small>
                            </div>

                            <label class="col-md-1 control-label mr20" for="admin-first-name">X</label>

                            <div class="col-md-3">
                                <input type="text" name="top_ads_height" class="form-control" value="{{ old('top_ads_height') ? old('top_ads_height') : Options::get('top_ads_height') }}" placeholder="Height" >
                                <small class="help-block text-danger">{{$errors->first('top_ads_height')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Bottom Advertisement</label>
                            <div class="col-md-3">
                                <input type="text" name="bottom_ads_width" class="form-control" value="{{ old('bottom_ads_width') ? old('bottom_ads_width') : Options::get('bottom_ads_width') }}" placeholder="Width" >
                                <small class="help-block text-danger">{{$errors->first('bottom_ads_width')}}</small>
                            </div>

                            <label class="col-md-1 control-label mr20" for="admin-first-name">X</label>

                            <div class="col-md-3">
                                <input type="text" name="bottom_ads_height" class="form-control" value="{{ old('bottom_ads_height') ? old('bottom_ads_height') : Options::get('bottom_ads_height') }}" placeholder="Height" >
                                <small class="help-block text-danger">{{$errors->first('bottom_ads_height')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Side Advertisement</label>
                            <div class="col-md-3">
                                <input type="text" name="side_ads_width" class="form-control" value="{{ old('side_ads_width') ? old('side_ads_width') : Options::get('side_ads_width') }}" placeholder="Width" >
                                <small class="help-block text-danger">{{$errors->first('side_ads_width')}}</small>
                            </div>

                            <label class="col-md-1 control-label mr20" for="admin-first-name">X</label>

                            <div class="col-md-3">
                                <input type="text" name="side_ads_height" class="form-control" value="{{ old('side_ads_height') ? old('side_ads_height') : Options::get('side_ads_height') }}" placeholder="Height" >
                                <small class="help-block text-danger">{{$errors->first('side_ads_height')}}</small>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                            </div>
                        </div>
                    </div>

                </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop