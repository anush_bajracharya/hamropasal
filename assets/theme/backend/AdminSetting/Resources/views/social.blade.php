@extends('backend.layouts.main')
@section('content')

    <div id="content-container">
        <ol class="breadcrumb">
            {!!$breadcrumbs!!}
        </ol>

        <div id="page-content" class="adminMgmtPageContent">

            @if(Session::get('success_message'))
                <div class="alert alert-success containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success_message') }}
                </div>
            @endif

            @if(Session::get('error_message'))
                <div class="alert alert-danger containerAlert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error_message') }}
                </div>
            @endif

            <div class="panel">
                <!--Panel heading-->
                @include('backend.common.panel-heading-settings')

                <!--Panel body-->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in">
                           <form action="{{ route('admin.setting.social.store') }}" method="POST" class="panel-body form-horizontal form-padding">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Facebook URL</label>
                            <div class="col-md-8">
                                <input type="text" name="facebook_url" class="form-control" value="{{ old('facebook_url') ? old('facebook_url') : Options::get('facebook_url') }}" placeholder="Facebook URL" >
                                <small class="help-block text-danger">{{$errors->first('facebook_url')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Twitter URL</label>
                            <div class="col-md-8">
                                <input type="text" name="twitter_url" class="form-control" value="{{ old('twitter_url') ? old('twitter_url') : Options::get('twitter_url') }}" placeholder="Twitter URL" >
                                <small class="help-block text-danger">{{$errors->first('twitter_url')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Google+ URL</label>
                            <div class="col-md-8">
                                <input type="text" name="google_plus_url" class="form-control" value="{{ old('google_plus_url') ? old('google_plus_url') : Options::get('google_plus_url') }}" placeholder="Google+ URL" >
                                <small class="help-block text-danger">{{$errors->first('google_plus_url')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Instagram URL</label>
                            <div class="col-md-8">
                                <input type="text" name="instagram_url" class="form-control" value="{{ old('instagram_url') ? old('instagram_url') : Options::get('instagram_url') }}" placeholder="Instagram URL" >
                                <small class="help-block text-danger">{{$errors->first('instagram_url')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Youtube URL</label>
                            <div class="col-md-8">
                                <input type="text" name="youtube_url" class="form-control" value="{{ old('youtube_url') ? old('youtube_url') : Options::get('youtube_url') }}" placeholder="Youtube URL" >
                                <small class="help-block text-danger">{{$errors->first('youtube_url')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">Viber Number</label>
                            <div class="col-md-8">
                                <input type="text" name="viber" class="form-control" value="{{ old('viber') ? old('viber') : Options::get('viber') }}" placeholder="Viber" >
                                <small class="help-block text-danger">{{$errors->first('viber')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="admin-first-name">WhatsApp Number</label>
                            <div class="col-md-8">
                                <input type="text" name="whatsapp" class="form-control" value="{{ old('whatsapp') ? old('whatsapp') : Options::get('whatsapp') }}" placeholder="WhatsApp Number" >
                                <small class="help-block text-danger">{{$errors->first('whatsapp')}}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-block btn-primary" name="submit" value="UPDATE">
                            </div>
                        </div>
                    </div>

                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop