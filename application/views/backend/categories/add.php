            <?php $this->load->view('backend/header') ?>
            

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                   
                    
					
					 <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Category Add</h3>
                                </div>
                    
                    
                                <!--Input Size-->
                                <!--===================================================-->
                                <form class="form-horizontal" enctype="multipart/form-data" method="POST" name="categoryAdd" action="<?php echo site_url('backend/categories/store'); ?>">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="demo-is-inputsmall">Category Name</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="name" placeholder=".input-sm" class="form-control input-sm" id="demo-is-inputsmall">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="demo-is-inputnormal" class="col-sm-3 control-label">Image</label>
                                           <div class="col-sm-6">
                                            <span class="pull-left btn btn-primary btn-file">
                                            Browse... <input type="file">
                                            </span>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="demo-is-inputlarge" class="col-sm-3 control-label">Status</label>
                                            <div class="col-sm-6">
                    
                                            <!-- Radio Buttons -->
                                            <div class="radio">
                                                <input id="demo-form-radio" class="magic-radio" name='status' type="radio" name="form-radio-button" checked="" value="1">
                                                <label for="demo-form-radio" >Acive</label>
                                            </div>
                                            <div class="radio">
                                                <input id="demo-form-radio-2" class="magic-radio"  name='status' type="radio" name="form-radio-button" value="0">
                                                <label for="demo-form-radio-2">Inactive</label>
                                            </div>
                                            
                                        </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="demo-is-inputlarge" class="col-sm-3 control-label">Description</label>
                                            <div class="col-sm-6">
                                            <textarea name='description' id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
                                        </div>
                                        </div>
                                       
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Add</button>
                                                <button class="btn btn-warning" type="reset">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!--===================================================-->
                                <!--End Input Size-->
                    
                    
                            </div>
                        </div>
					
					
                </div>
                <!--===================================================-->
                <!--End page content-->

                    </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->

                   

<?php $this->load->view('backend/mainnav') ?>


           
            <?php $this->load->view('backend/footer') ?>

            
            

            


        

        

        