            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">

                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">

                                <!--Profile Widget-->
                                <!--================================-->
                                <div id="mainnav-profile" class="mainnav-profile">
                                    <div class="profile-wrap">
                                        <div class="pad-btm">
                                            <span class="label label-success pull-right">New</span>
                                            <img class="img-circle img-sm img-border" src="<?php echo base_url() ?>assets/theme/backend/img/profile-photos/1.png" alt="Profile Picture">
                                        </div>
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                            <p class="mnp-name">Aaron Chavez</p>
                                            <span class="mnp-desc">aaron.cha@themeon.net</span>
                                        </a>
                                    </div>
                                    <div id="profile-nav" class="collapse list-group bg-trans">
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#" class="list-group-item">
                                            <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                                        </a>
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#" class="list-group-item">
                                            <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                                        </a>
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#" class="list-group-item">
                                            <i class="demo-pli-information icon-lg icon-fw"></i> Help
                                        </a>
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#" class="list-group-item">
                                            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                        </a>
                                    </div>
                                </div>


                                <!--Shortcut buttons-->
                                <!--================================-->
                                <div id="mainnav-shortcut">
                                    <ul class="list-unstyled">





                                        <li class="col-xs-3" data-content="My Profile">
                                            <a class="shortcut-grid" href="<?php echo base_url() ?>assets/theme/backend/#">
                                                <i class="demo-psi-male"></i>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Messages">
                                            <a class="shortcut-grid" href="<?php echo base_url() ?>assets/theme/backend/#">
                                                <i class="demo-psi-speech-bubble-3"></i>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Activity">
                                            <a class="shortcut-grid" href="<?php echo base_url() ?>assets/theme/backend/#">
                                                <i class="demo-psi-thunder"></i>
                                            </a>
                                        </li>
                                        <li class="col-xs-3" data-content="Lock Screen">
                                            <a class="shortcut-grid" href="<?php echo base_url() ?>assets/theme/backend/#">
                                                <i class="demo-psi-lock-2"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--================================-->
                                <!--End shortcut buttons-->


                                <ul id="mainnav-menu" class="list-group">
						
						            <!--Category name-->
						            <li class="list-header">Navigation</li>
						
						            <!--Menu list item-->
						            <li class="active-link">
						                <a href="<?php echo base_url() ?>assets/theme/backend/index.html">
						                    <i class="demo-psi-home"></i>
						                    <span class="menu-title">
												<strong>Dashboard</strong>
											</span>
						                </a>
						            </li>
						
						            <!--Menu list item-->
						            <li>
						                <a href="<?php echo base_url() ?>assets/theme/backend/#">
						                    <i class="demo-psi-split-vertical-2"></i>
						                    <span class="menu-title">
												<strong>Categories</strong>
											</span>
											<i class="arrow"></i>
						                </a>
						
						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="<?php echo base_url() ?>assets/theme/backend/layouts-collapsed-navigation.html">List Categories</a></li>
											<li><a href="<?php echo base_url() ?>assets/theme/backend/layouts-offcanvas-navigation.html">Add Categories</a></li>
                                            
											
											
						                </ul>
						            </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>assets/theme/backend/#">
                                            <i class="demo-psi-split-vertical-2"></i>
                                            <span class="menu-title">
                                                <strong>Product</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                        
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="<?php echo base_url() ?>assets/theme/backend/layouts-collapsed-navigation.html">List Product</a></li>
                                            <li><a href="<?php echo base_url() ?>assets/theme/backend/layouts-offcanvas-navigation.html">Add Product</a></li>
                                            
                                            
                                            
                                        </ul>
                                    </li>
						
						            <!--Menu list item-->
						           
						
						            <li class="list-divider"></li>
						
						           
                                        </ul>
                                    </div>
                                </div>
                                <!--================================-->
                                <!--End widget-->

                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>