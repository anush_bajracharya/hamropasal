            <?php $this->load->view('backend/header') ?>


            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Input size</h3>
                            </div>


                            <!--Input Size-->
                            <!--===================================================-->
                            <form class="form-horizontal">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-is-inputsmall">Product Key</label>
                                        <div class="col-sm-6">
                                            <input type="text" placeholder=".input-sm" class="form-control input-sm" id="demo-is-inputsmall">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="demo-is-inputnormal" class="col-sm-3 control-label">Image</label>
                                        <div class="col-md-6">
                                            <span class="pull-left btn btn-primary btn-file">
                                                Browse... <input type="file">
                                            </span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="demo-is-inputlarge" class="col-sm-3 control-label">Description</label>
                                        <div class="col-md-6">
                                            <textarea id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">On Sale</label>
                                        <div class="col-md-6">
                                            <div class="radio">


                                                <input id="demo-form-radio" class="magic-radio" type="radio" name="form-radio-button" checked="" value="1">
                                                <label for="demo-form-radio">Yes</label>
                                            </div>
                                            <div class="radio">
                                                <input id="demo-form-radio-2" class="magic-radio" type="radio" name="form-radio-button" value="0">
                                                <label for="demo-form-radio-2">No</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Is Discount</label>
                                        <div class="col-md-6">

                                            <!-- Radio Buttons -->
                                            <div class="radio">
                                                <input id="demo-form-radio" class="magic-radio" type="radio" name="form-radio-button" checked="" value="1">
                                                <label for="demo-form-radio">Yes</label>
                                            </div>
                                            <div class="radio">
                                                <input id="demo-form-radio-2" class="magic-radio" type="radio" name="form-radio-button" value="0">
                                                <label for="demo-form-radio-2">No</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="demo-text-input">Discounted Price</label>
                                        <div class="col-md-6">
                                            <input type="text" id="demo-text-input" class="form-control" placeholder="Text">

                                        </div>
                                    </div>




                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Login</button>
                                                <button class="btn btn-warning" type="reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--===================================================-->
                            <!--End Input Size-->


                        </div>
                    </div>

                </div>


            </div>
            <!--===================================================-->
            <!--End page content-->

            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <?php $this->load->view('backend/mainnav') ?>



            <?php $this->load->view('backend/footer') ?>