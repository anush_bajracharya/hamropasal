<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();


		$this->load->library('form_validation');
		$this->load->library('session');

		$this->load->model('CategoryModel');


		$this->categories = new CategoryModel;
	}

	public function index()
	{
		$this->load->view('backend/categories/list');
	}

	public function add()
	{
		$this->load->view('backend/categories/add');
	}
	public function store()
	{
		$this->form_validation->set_rules('name', 'Category Name', 'required');
		// $this->form_validation->set_rules('description', 'Description', 'required');


		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url('categories/add'));
		} else {
			$data = [
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status'),
			];
			$this->categories->insert_item($data);
			redirect(base_url('categories'));
		}
	}
}
